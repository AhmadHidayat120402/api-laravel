<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use function PHPUnit\Framework\returnSelf;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $products = DB::table('product')->get();
        return view('products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {

        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $data = [
            'name' => $request->input('name'),
            'price' => $request->input('price'),
            'description' => $request->input('description'),
        ];

        DB::table('products')->insert($data);
        return redirect()->route('products.index')->with('success product created successfully');
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $product = DB::table('products')->find($id);
        return view('products.edit', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $product =  DB::table('products')->find($id);
        return view('products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request,  $id)
    {
        $data = [
            'name' => $request->input('name'),
            'price' => $request->input('price'),
            'description' => $request->input('description'),
        ];

        DB::table('products')->where('id', $id)->update($data);

        return redirect()->route('products.index')->with('success products updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $product = DB::table('products')->where('id', $id)->delete();
        return redirect()->route('products.index')->with('success produst deleted successfully');
    }

    public function test()
    {
        $id = request()->route('id');
        $nama = request()->route('nama');
        return "halo nomer $id dengan nama $nama";
    }
}
