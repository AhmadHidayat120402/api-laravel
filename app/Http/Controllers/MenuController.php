<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MenuController extends Controller
{
    public function index()
    {
        return ["kayu putih", "teh gelas", "makarina"];
    }

    public function create()
    {
        return "your data is create";
    }

    public function update()
    {
        return "your data is updated";
    }

    public function delete()
    {
        return "your data is deleted";
    }
}
