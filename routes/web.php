<?php

use App\Http\Controllers\MenuController;
// use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// route parameter
// Route::get('/user/{id}', function () {
//     $id = request()->route('id');
//     return "hello nomer " . $id;
// });

// query parameter
// Route::get('/customer', function () {
//     $search = request()->query("search");
//     return "hello nomer " . $search;
// });
// Route::prefix('admin')->group(
//     Route::get('/customer/{id}/{nama}', [ProductController::class, 'test'])

// );



Route::prefix('products')->group(function () {
    Route::get('/', [ProductController::class, 'index'])->name('products.index');
    Route::get('/create', [ProductController::class, 'create'])->name('products.create');
    Route::post('/store', [ProductController::class, 'store'])->name('products.store');
    Route::get('/{id}', [ProductController::class, 'show'])->name('products.show');
    Route::get('/{id}/edit', [ProductController::class, 'edit'])->name('products.edit');
    Route::put('/{id}', [ProductController::class, 'update'])->name('products.update');
    Route::delete('/{id}', [ProductController::class, 'destroy'])->name('products.destroy');
});


Route::prefix('menu')->group(
    Route::get('/', [MenuController::class, 'index']),
    Route::post('/', [MenuController::class, 'create']),
    Route::put('/{id}', [MenuController::class, 'update']),
    Route::delete('/{id}', [MenuController::class, 'delete']),

);
